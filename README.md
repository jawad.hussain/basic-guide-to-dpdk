# Basic Guide to DPDK

## Setting up KVM on Ubuntu:

Since DPDK involves communication between two devices we will be using virtual machines for our examples.

1. To install KVM follow the link : https://www.tecmint.com/install-kvm-on-ubuntu/  
2. To setup a virtual machine on KVM follow the link : https://www.tecmint.com/create-virtual-machines-in-kvm-using-virt-manager/  


## Setting up Hugepages for DPDK:

**NOTE:** Setting up Hugepages can also be seen from : https://www.thegeekdiary.com/centos-rhel-67-how-to-configure-hugepages/

1. Edit /etc/sysctl.conf file and specify the number of hugepages in the nr_hugepages parameter. The entry makes the parameter persist across reboots but does not come into effect until you run the ‘sysctl -p’ command described in the next step.

   ```
   vi /etc/sysctl.conf
   ```
   paste the following line in the file just opened

   ```
   vm.nr_hugepages = 256
   ```
   <br/>
2. Execute ‘sysctl -p’ command to enable the hugepages parameter.
    ```
    sysctl -p
    ```
    *It is recommended to restart the system afterwards*
    
    <br/>
3. To verify check for file /proc/meminfo:
   ```
   cat /proc/meminfo | grep Huge
   ``` 
    <br/>

## Installing DPDK:

**NOTE:** All of this can be found on offical link : https://core.dpdk.org/doc/quick-start/

1. First download DPDK from https://core.dpdk.org/download/
   
2. Extract the downloaded file
   
    ```
    tar xf dpdk.tar.gz

    cd dpdk
    ```
    Make sure the file name in the command is the same as that of the downloaded file.
   <br/> 
3. Build libraries, drivers and test application
    
    ```
    meson build

    ninja -C build
    ```
    To include the examples as part of the build, replace the meson command with
    

    ```
    meson -Dexamples=all build
    ``` 
    <br/> 
4.  Reserve huge pages memory
   
    ```
    mkdir -p /dev/hugepages

    mountpoint -q /dev/hugepages || mount -t hugetlbfs nodev /dev/hugepages

    echo 64 > /sys/devices/system/node/node0/hugepages/hugepages-2048kB/nr_hugepages
    ```
    <br/>
## Running Basic Applications:

### 1. Hello world app:
-   Run the followning command form terminal in the downloaded dpdk folder:
    
    ```
    sudo ./bulid/example/dpdk-helloworld
    ```

### 2. L2FWD:
- First we need to do port mapping. We will be using igb_uio. Run the following commands:
  
  ```
    sudo apt-get install dpdk-kmods-dkms
    
    sudo modprobe igb_uio

    sudo ./usertools/dpdk-devbind.py -s

    sudo ./usertools/dpdk-devbind.py --force -b igb_uio 0000:01:00.0

    sudo ./usertools/dpdk-devbind.py -s
  ```
    `./usertools/dpdk-devbind.py -s` is used to check status of devbind. It can be skipped.    
    You dont need to map it again for each application. Once is enough.

- Now we can run l2fwd:

    
    ```
    sudo ./dpdk-l2fwd -- -p 1 -q 1
    ```
    `-p` repersents number of ports  
    `-q` repersents nouber of queues


### 3.TestPmd:
**NOTE:** This example is taken from https://youtu.be/Un5-AN4nb9s  
For further detail refer the above link.


- To Run testpmd on transmitting device we need to do port mapping as shown above in L2FWD:
  
    ```
    sudo apt-get install dpdk-kmods-dkms
    
    sudo modprobe igb_uio

    sudo ./usertools/dpdk-devbind.py -s

    sudo ./usertools/dpdk-devbind.py --force -b igb_uio 0000:01:00.0

    sudo ./usertools/dpdk-devbind.py -s
  ```
    `./usertools/dpdk-devbind.py -s` is used to check status of devbind. It can be skipped.  
    You dont need to map it again for each application. Once is enough.

- Start testpmd in interactive mode:

    ```
    sudo ./build/app/dpdk-testpmd -- -i
    ```
- ***NOTE:** All the following commands will be run inside testpmd interative mode:*  


- For this example we will be using port 0 and port 1 to transmit data.

- Create a bonded device on port 1.

    ```
    create bonded device 1 0
    ```
 - Making port 0 slave of port 1:
 
    ```
    add bonding slave 0 1
    ```

- Setting bonding mode:

    ```
    set bonding mode 4 1
    ```

- Setting forwarding mode to transmission only:

    ```
    set fwd txonly
    ```

- Starting ports:
    
    ```
    port start 0

    port start 1
    ```
- To see status of port 1:

    ```
    show port info 1
    ```

- Setting connection to recieving device:

    **NOTE:** last argument of following commands must be mac address of the recieving device. The mac addresses in this example are random.

    ```
    set eth-peer 0 e8:ea:6a:06:1b:1a

    set eth-peer 1 e8:ea:6a:06:1b:1a
    ```

- Starting the connection and sending packets:
    
    ```
    start
    ```

- To view port stats:

    ```
    show port stats 2
    ```

- To stop sending data:

    ```
    stop
    ```
- To view recieved packets on recieving device run l2fwd on it. Or follow the link : https://youtu.be/kAcAwGUMrqo